<!DOCTYPE html>
<html lang="en">

<head>
    @include('inc.head')
</head>

<body class="hold-transition sidebar-mini">
<div class="wrapper">

    @include("inc.main_header")


    @include("inc.sidebar")

    @yield("content")


    @include("inc.footer")


</div>

@include('inc.script')

</body>
</html>
