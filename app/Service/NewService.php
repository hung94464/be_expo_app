<?php
namespace App\Service;

use App\Models\News;
use Illuminate\Http\Request;

class NewService{
    public static function store(Request $request){
        $news = News::updateOrCreate(
            ["slug" => $request["slug"]],
            [
                "title" => $request["title"],
                "slug" => $request["slug"],
                "description" => $request["description"],
                "image" => $request["image"],
                "content" => $request["content"],
                "category_id" => $request["category"],
                "status" => $request["status"]
            ]
        );
    }
}
