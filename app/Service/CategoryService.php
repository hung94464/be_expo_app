<?php
namespace App\Service;

use App\Models\Category;

class CategoryService{
    public static function getCategories(){
        return Category::all()->toArray();
    }
}
