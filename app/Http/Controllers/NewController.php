<?php

namespace App\Http\Controllers;

use App\Service\CategoryService;
use App\Service\NewService;
use Illuminate\Http\Request;

class NewController extends Controller
{
    public function index()
    {
        //
    }

    public function create()
    {
        $categories = CategoryService::getCategories();
        return view("news.create" ,[
            "title" => "Create News",
            "categories" => $categories,
        ]);
    }

    public function store(Request $request)
    {
        NewService::store($request);
        return redirect()->back()->with("success", "Create successfully !");
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
